<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
header("Content-Type:text/html; charset=utf-8");
require_once "../index/config.php";

$id=$_SESSION['result']['m_name'];
$status = $_POST['m_status'];//請求
$m_id = $_POST['m_id'];

$sql0 = "SELECT m_status FROM member WHERE m_id = :id";
$result0 = $db_link->prepare($sql0);
$result0->bindValue(":id",$m_id ,PDO::PARAM_INT);
$result0->execute();
$row0=$result0->fetchAll();
$judge=$row0[0][0];
echo $judge; //資料表內的狀態
if($judge==$status){
    $judge=$status;
}else{
    if($judge==0 && $status==1){
        $judge=1;
    }else if($judge==1 && $status==0){
        $judge=0;
    }
}

$sql = "SELECT * FROM member WHERE m_name = :id ";
$result = $db_link->prepare($sql);
$result->bindValue(":id",$id,PDO::PARAM_STR);
$result->execute();
if($row = $result->fetchAll(PDO::FETCH_BOTH)>0){
   $sql1="UPDATE member SET m_status = ?  WHERE m_id = ?";
   $result1 = $db_link->prepare($sql1);
   $result1->execute(array($judge,$m_id));
}

?>