<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>上傳新品</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
          .nav-item {

            font-size: 22px !important;
        }

        .item {
            margin: 10px 0 10px 0;
        }

        .table {
            text-align: center;

        }

        .row1 {
            margin: 0 0 30px 0;
        }

        .btn {
            font-size: 24px;
            float: right;
        }

        .btn1 {
            font-size: 18px;
            float: right;
        }

        .a {
            padding: 0 0 40px 0;

        }
    </style>
</head>

<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">XILFTEN 後臺管理系統</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="../view/logout.php">登出</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">

            <nav class="col-md-2 d-none d-md-block bg-light sidebar mt-5">
                <h3 class="mt-4">管理員編號:<?php echo $_SESSION['result']['m_name'] ?></h3>
                <span hidden id="id"><?php echo $_SESSION['result']['m_name'] ?></span>
                <div class="sidebar-sticky">
                    <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">


                        <span>功能列表</span>

                    </h5>
                    <ul class="nav flex-column">
                        <li class="nav-item item">
                            <a class="nav-link active" href="bar.php">
                                <i class="las la-user"></i>
                                會員帳號管理
                            </a>
                        </li>
                        <li class="nav-item item">
                            <a class="nav-link" href="barGet.php">
                                <i class="las la-paste"></i>
                                出貨單管理
                            </a>
                        </li>
                        <li class="nav-item item">
                            <a class="nav-link" href="monthPrice.php">
                                <i class="las la-dollar-sign"></i>
                                銷售額管理
                            </a>
                        </li>
                        <li class="nav-item item">
                            <a class="nav-link" href="putOn.php">
                                <i class="las la-chevron-circle-up"></i>
                                上傳新品
                            </a>
                        </li>
                    </ul>


                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-5">

                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h1">上傳新品</h1>
                </div>
                <div id="tab">
                    <div class="row row1">
                        <div class="col-6 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default">商品名稱</span>
                            </div>
                            <input type="text" id="name" class="form-control" placeholder="復仇者聯盟" required>
                        </div>

                        <div class="col-6 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default">語言分類</span>
                            </div>
                            <input type="text" id="language" class="form-control" placeholder="英語中字" required>
                        </div>
                    </div>
                    <div class="row row1">
                        <div class="col-12 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default">演員名單</span>
                            </div>
                            <textarea type="text" id="actor" class="form-control" required placeholder="小勞勃·道尼,克里斯·伊凡,馬克·盧法洛,克里斯·漢斯沃,史嘉蕾·喬韓森..."></textarea>
                        </div>
                    </div>
                    <div class="row row1">
                        <div class="col-3 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default">商品類型</span>
                            </div>
                            <select id="object" class="form-control form-control-lg" required>
                                <option>動作與冒險</option>
                                <option>喜劇</option>
                                <option>恐怖</option>
                                <option>劇情</option>
                                <option>紀錄片</option>
                            </select>
                        </div>
                        <div class="col-3 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default">時間長度(分鐘)</span>
                            </div>
                            <input type="text" id="time" class="form-control" placeholder="輸入數字" required>
                        </div>
                        <div class="col-3 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text">上映年份</span>
                            </div>
                            <input type="text" id="year" class="form-control" placeholder="輸入年份" required>
                        </div>
                        <div class="col-3 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default">價格設定</span>
                            </div>
                            <input type="text" id="price" class="form-control" placeholder="輸入價格" required>
                        </div>
                    </div>
                    <div class="row row1">
                        <div class="col-12 input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputGroup-sizing-default">劇情大綱</span>
                            </div>
                            <textarea type="text" id="story" class="form-control" rows="5" required placeholder="復仇者聯盟系列最終章！灰飛煙毀後的世界該如何走下去，是否還有逆轉結局的機會？一切都將在此揭曉。 接續《復仇者聯盟3：無限之戰》，薩諾斯彈指間毀滅宇宙一半的生物後，僅存的復仇者們要如何重整旗鼓，背水一戰，為僅存的信念而戰。在《復仇者聯盟3：無限之戰》中未現身的鷹眼、蟻人，皆在本片登場，《驚奇隊長》也加入戰局。"></textarea>
                        </div>
                    </div>
                    <div class="row row1">
                        <div class="col-7">
                            <div class="row">
                                <div class="a col-9 input-group mb-3 ">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">上傳圖片</span>
                                    </div>
                                    <form method="POST" action="putFile.php" id="myform" enctype="multipart/form-data"">
                                        <div class="custom-file">
                                            <input type="file" name='img' id="img" class="custom-file-input" accept="image/*" required>
                                            <label class="custom-file-label" for="inputGroupFile01">選擇檔案後請按上傳檔案</label>
                                        </div>

                                </div>
                                <div class="col-3 pr-5 pl-0">
                                    <button type="submit" id="imgput" class="btn btn-primary btn1">上傳圖片</button>
                                </div>
                                </form>
                                <div class="a col-12 input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-default">商品標題</span>
                                    </div>
                                    <select id="sence" class="form-control" required>
                                        <option>最新推薦</option>
                                        <option>本周推薦</option>
                                        <option>本周熱門</option>
                                        <option>本周新片</option>
                                    </select>
                                </div>

                                <div class="a col-12 input-group input-group-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" style="height:48px;">嵌入YOUTUBE影片網址</span>
                                    </div>
                                    <input type="text" id="film" class="form-control" placeholder="iframe 格式" required>
                                </div>
                            </div>

                        </div>
                        <div class="col-5">
                            <div class="row row1">
                                <div class="col-12">
                                    <img id="imgshow" width="80%" height="400px" />
                                </div>
                            </div>
                            <button class="btn btn-primary" type="button" id="submit">送出</button>
                        </div>
                    </div>
                    
                </div>
            </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        //在input file內容改變的時候觸發事件
        $('#img').change(function() {
            //獲取input file的files檔案陣列;
            //$('#filed')獲取的是jQuery物件，.get(0)轉為原生物件;
            //這邊預設只能選一個，但是存放形式仍然是陣列，所以取第一個元素使用[0];
            var file = $('#img').get(0).files[0];
            //建立用來讀取此檔案的物件
            var reader = new FileReader();
            //使用該物件讀取file檔案
            reader.readAsDataURL(file);
            //讀取檔案成功後執行的方法函式
            reader.onload = function(e) {
                //讀取成功後返回的一個引數e，整個的一個進度事件
                console.log(e);
                //選擇所要顯示圖片的img，要賦值給img的src就是e中target下result裡面
                //的base64編碼格式的地址
                $('#imgshow').get(0).src = e.target.result;
            }
            console.log($("#img").val());
        })
    </script>
    <script>
        $("#submit").click(function() {
            name = $("#name").val(); //商品名稱
            language = $("#language").val(); //語言分類
            actor = $("#actor").val(); //演員名單
            object = $("#object").val(); //商品類型
            time = $("#time").val(); //時間長度
            year = $("#year").val(); //上映年份
            price = $("#price").val(); //價格設定
            story = $("#story").val(); //劇情大綱
            sence = $("#sence").val(); //商品標題
            img = $("#img").val();
            film = $("#film").val(); //商品預告

            if (name == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (language == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (actor == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (object == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (time == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (year == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (price == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (story == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (sence == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (img == "") {
                swal("警告!", "有欄位未填", "warning");
            } else if (film == "") {
                swal("警告!", "有欄位未填", "warning");
            }
            if (name != "" && language != "" && actor != "" && object != "" && time != "" && year != "" && price != "" && story != "" && sence != "" && img != "" && film != "") {
                $.ajax({
                    type: 'POST',
                    url: '../bar/objectInsert.php',
                    // dataType: 'json',
                    data: {
                        name: name,
                        language: language,
                        actor: actor,
                        object: object,
                        time: time,
                        year: year,
                        price: price,
                        story: story,
                        sence: sence,
                        img: img,
                        film: film,
                    },
                    success: function(data) {
                        console.log(data);
                        // console.log("success");
                        // console.log(typeof(data));
                        // document.write(data);

                        swal("新增成功!", "資料已進入資料庫", "success");
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.log(typeof(data));
                    }
                });

            }

        })

      
    </script>


</body>


</html>