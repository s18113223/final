<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
header("Content-Type:text/html; charset=utf-8");
require_once "../index/config.php";
ini_set('display_errors','off');    # 關閉錯誤輸出

$id = $_POST['did'];
// echo $id;
$sql = "SELECT * FROM member WHERE m_name = ?";
$result = $db_link->prepare($sql);
$result->execute($id);
if ($row = $result->fetchAll() > 0) {
    $sql1 = "SELECT * FROM member ORDER BY m_status Desc ";
    $result1 = $db_link->prepare($sql1);
    $result1->execute();
    $row1 = $result1->fetchAll(PDO::FETCH_BOTH);
   
    $num = count($row1);
    
    for ($i = 0; $i < $num; $i++) {
        $array[] = array(
            "會員ID" => $row1[$i][0],
            "會員暱稱" => $row1[$i][1],
            "會員帳號" => $row1[$i][2],
            "會員電郵" => $row1[$i][4],
            "會員權限" => $row1[$i][5]

        );
    }
    // echo count($array); 
    
    $dataJson = json_encode($array, JSON_UNESCAPED_UNICODE);
   
    echo $dataJson;
    // echo gettype($dataJson); 
}
