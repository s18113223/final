<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>出貨單管理</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        .nav-item {

            font-size: 22px !important;
        }

        .item {
            margin: 10px 0 10px 0;
        }
        .table {
        text-align: center;
        
    }

    </style>
</head>

<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">XILFTEN 後臺管理系統</a>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="../view/logout.php">登出</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">

             <nav class="col-md-2 d-none d-md-block bg-light sidebar mt-5">
                <h3 class="mt-4">管理員編號:<?php echo $_SESSION['result']['m_name'] ?></h3>
                <span hidden id="id"><?php echo $_SESSION['result']['m_name'] ?></span>
                <div class="sidebar-sticky">
                    <h5 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">


                        <span>功能列表</span>

                    </h5>
                    <ul class="nav flex-column">
                        <li class="nav-item item">
                            <a class="nav-link active" href="bar.php">
                                <i class="las la-user"></i>
                                會員帳號管理
                            </a>
                        </li>
                        <li class="nav-item item">
                            <a class="nav-link" href="barGet.php">
                                <i class="las la-paste"></i>
                                出貨單管理
                            </a>
                        </li>
                        <li class="nav-item item">
                            <a class="nav-link" href="monthPrice.php">
                                <i class="las la-dollar-sign"></i>
                                銷售額管理
                            </a>
                        </li>
                        <li class="nav-item item">
                            <a class="nav-link" href="putOn.php">
                                <i class="las la-chevron-circle-up"></i>
                                上傳新品
                            </a>
                        </li>
                    </ul>


                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 mt-5">

                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h1">出貨單管理</h1>
                </div>
                <div id="tab">

                </div>
            </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            let id = $('#id').text();
            console.log(id)
            $.ajax({
                type: 'POST',
                url: '../bar/memberGet.php',
                dataType: 'json',
                data: {
                    did: id,
                },
                success: function(data) {
                    console.log(data);
                    console.log("success");
                    console.log(typeof(data));
                    // document.write(data);
                    var htm = "";
                    htm += "<table  class='table table - striped '><thead><tr><th>訂單編號</th><th>貨物編號</th><th>貨物名稱</th>"
                        +"<th>購買人編號</th><th>購買人姓名</th><th>貨品數量</th><th>下單日期</th><th>操作</th></tr></thead><tbody>";
                    var item = data;
                    // console.log(item.length);
                    // console.log(item[0]["電影名稱"]);
                    for (var i = 0; i < item.length; i++) {
                        
                        htm += '<tr><td>' +
                            item[i]["訂單編號"] + '</td ><td>' +
                            item[i]["貨物編號"] + '</td><td>' +
                            item[i]["貨物名稱"] + '</td><td>' +
                            item[i]["購買人編號"] + '</td><td>' +
                            item[i]["購買人姓名"] + '</td><td>' +
                            item[i]["貨品數量"] + '</td><td>' +
                            item[i]["下單日期"] + '</td><td>' +
                            '<button type="button" class="btn btn-primary" id="'+item[i]["訂單編號"] +'" value='+item[i]["出貨狀態"]+' onclick="myFunction(this);">出貨</button>'+
                            '</td></tr>';
                            
                    }
                    htm += '</tbody></table>';
                    $("#tab").html(htm);

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(typeof(data));
                }

            });
           


        })
    </script>
   <script>
        function myFunction(e) {
            x = $(e).val();
            y=$(e).attr("id");
            console.log(x);
            
            console.log($(e).attr("id"));
            $.ajax({
            type: 'POST',
            url: 'ship.php',
            data: {
                c_status: x,
                c_id:y,
            },
            success: function(data) {
                console.log("成功");
                // document.write(data);
                
                swal("操作成功!", "訂單已出貨", "success");
                // document.location.reload();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log(typeof(textStatus));
            }

        });
        }
    </script>

</body>


</html>