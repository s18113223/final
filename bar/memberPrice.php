<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
header("Content-Type:text/html; charset=utf-8");
require_once "../index/config.php";
$id = $_POST['did'];
$sql = "SELECT * FROM member WHERE m_name = :id";
$result = $db_link->prepare($sql);
$result->bindValue(":id",$id,PDO::PARAM_STR);
$result->execute();
$key=1;
if($row = $result->fetchAll(PDO::FETCH_BOTH)>0){
    $sql1="SELECT cart.c_date,cart.f_id,film.f_name,sum(cart.c_count),sum(cart.c_count)*film.f_price
           FROM cart,film,member
           WHERE c_shipStatus = :c_shipStatus AND MONTH(NOW()) = MONTH(cart.c_date) AND film.f_id = cart.f_id AND member.m_id=cart.m_id
           GROUP BY film.f_id";
    $result1 = $db_link->prepare($sql1);
    $result1->bindValue(":c_shipStatus",$key,PDO::PARAM_INT);
    $result1->execute();
    $row1 = $result1->fetchAll(PDO::FETCH_BOTH);
    $num = count($row1);
    for ($i = 0; $i < $num; $i++) {
        $array1[] = array(
            "下單日期" => $row1[$i][0],
            "貨物編號" => $row1[$i][1],
            "貨物名稱" => $row1[$i][2],
            "總計數量" => $row1[$i][3],
            "總計金額" => $row1[$i][4],
        );
    }
    // echo count($array); 
    $dataJson = json_encode($array1, JSON_UNESCAPED_UNICODE);
    echo $dataJson;
}



?>