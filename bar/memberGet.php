<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
header("Content-Type:text/html; charset=utf-8");
require_once "../index/config.php";
$id = $_POST['did'];
$sql = "SELECT * FROM member WHERE m_name = :id";
$result = $db_link->prepare($sql);
$result->bindValue(":id",$id,PDO::PARAM_STR);
$result->execute();


$key=2;
if($row = $result->fetchAll(PDO::FETCH_BOTH)>0){
    $sql1 = "SELECT cart.c_id,cart.f_id,film.f_name,cart.m_id,member.m_name,cart.c_count,cart.c_date,cart.c_shipStatus
             FROM cart,member,film 
             WHERE c_status = :cstatus AND film.f_id = cart.f_id AND member.m_id=cart.m_id AND c_shipStatus = 0";   
    $result1 = $db_link->prepare($sql1);
    $result1->bindValue(":cstatus",$key,PDO::PARAM_INT);
    $result1->execute();
    $row1 = $result1->fetchAll(PDO::FETCH_BOTH);
    // print_r($row1);
    $num = count($row1);
    
    for ($i = 0; $i < $num; $i++) {
        $array[] = array(
            "訂單編號" => $row1[$i][0],
            "貨物編號" => $row1[$i][1],
            "貨物名稱" => $row1[$i][2],
            "購買人編號" => $row1[$i][3],
            "購買人姓名" => $row1[$i][4],
            "貨品數量" => $row1[$i][5],
            "下單日期" => $row1[$i][6],
            "出貨狀態" => $row1[$i][7]

        );
    }
    // echo count($array); 
    
    $dataJson = json_encode($array, JSON_UNESCAPED_UNICODE);
   
    echo $dataJson;
}


?>