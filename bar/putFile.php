<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>

<body>
    <?php
    $imgname = $_FILES['img']['name'];
    $tmp = $_FILES['img']['tmp_name'];
    $filepath = '../view/img/';
    if (move_uploaded_file($tmp, $filepath . $imgname)) {
        echo "<script>swal('成功!', '資料已進入資料庫 請繼續填寫資料', 'success');</script>";
        echo '<script>setTimeout(function(){
            history.go(-1)
          },1000);</script>';
    } else {
        echo "<script>swal('失敗!', '上傳失敗 請重新檢查', 'error');</script>";
        echo '<script>setTimeout(function(){
            history.go(-1)
          },1000);</script>';
    }
    ?>
</body>

</html>