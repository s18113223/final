-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主機： localhost
-- 產生時間： 2020 年 06 月 12 日 22:27
-- 伺服器版本： 10.1.44-MariaDB-0ubuntu0.18.04.1
-- PHP 版本： 7.3.18-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `finaltest`
--

-- --------------------------------------------------------

--
-- 資料表結構 `cart`
--

CREATE TABLE `cart` (
  `c_id` int(255) NOT NULL,
  `f_id` int(11) NOT NULL,
  `m_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `c_count` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 傾印資料表的資料 `cart`
--

INSERT INTO `cart` (`c_id`, `f_id`, `m_id`, `c_count`) VALUES
(1, 2, '3', 0),
(2, 4, '3', 0),
(14, 1, '12', 0),
(15, 3, '12', 1),
(16, 3, '12', 1),
(17, 3, '12', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `film`
--

CREATE TABLE `film` (
  `f_id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `f_actor` varchar(255) NOT NULL,
  `f_ language` varchar(255) NOT NULL,
  `f_type` varchar(255) NOT NULL,
  `f_time` varchar(255) NOT NULL,
  `f_year` varchar(255) NOT NULL,
  `f_note` text NOT NULL,
  `f_price` int(11) NOT NULL,
  `f_img` varchar(255) DEFAULT NULL,
  `f_rank` varchar(255) NOT NULL,
  `f_media` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 傾印資料表的資料 `film`
--

INSERT INTO `film` (`f_id`, `f_name`, `f_actor`, `f_ language`, `f_type`, `f_time`, `f_year`, `f_note`, `f_price`, `f_img`, `f_rank`, `f_media`) VALUES
(1, '絕地戰警', '馬汀·勞倫斯,威爾·史密斯,蒂雅·李歐妮,傑奇·卡尤,泰瑞莎·藍道,喬·潘托利亞諾', '英語中字', '動作與冒險', '123', '2020', '《絕地戰警》系列電影自1995年問世以來，靠著喜劇元素與兩位主角幹話無極限的超強默契，再融入暢快刺激及拳拳到肉的動作場面，成功風靡全球創下逾4.14億美元（約新台幣128億元）的傲人票房。本系列除了讓威爾史密斯與馬汀勞倫斯交出大銀幕的代表作外，威爾史密斯更靠著本片躋身好萊塢一線巨星的行列。暌違17年系列電影重起上路，影迷們除可看到兩位巨星賣弄身手外，兩位大叔還得對上由凡妮莎哈金斯所率領的新世代警探的挑釁，新舊世代究竟會擦出什麼樣的動作火花？', 350, 'img\\img1.jpg', '<i class=\"las la-award\"></i>本周排行第6名', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/XVoE16w6B_g\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(2, '猛禽小隊: 小丑女大解放', '瑪格·羅比,瑪麗·伊莉莎白·文斯蒂德,朱妮·絲莫利特-貝爾,蘿西·培瑞茲,克里斯·梅希納,艾拉·傑伊·巴斯科,黃艾麗,伊旺·麥奎格', '英語中字', '動作與冒險', '108', '2020', '你可曾聽過一個關於警察、金髮女郎、精神病患與黑幫公主的故事？《猛禽小隊: 小丑女大解放》是一段由哈莉親身演繹的曲折離奇的故事。高譚市最窮兇惡極的反派「黑面具」羅曼賽恩尼斯，與他的虐待狂得力助手薩斯，鎖定了一個名叫卡絲的年輕女孩為目標，翻遍高譚市的醜惡角落也勢必找到她。「小丑女」哈莉與女獵手、黑金絲雀及蕾妮蒙托亞的命運因此交會，原本不太可能聯手的四人也別無選擇，只能攜手合作打倒羅曼。', 350, 'img\\img2.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/2DZ9zEGN3kY\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(3, '全面攻佔3：天使救援', '傑瑞德·巴特勒, 摩根·費里曼', '英語中字', '動作與冒險', '120', '2019', '在上一起事件發生兩年後，當年的美國副總統艾倫·川布已成為了現任總統，麥克·班寧也已有了自己的家庭且即將被任命接任特勤局局長。看似家庭事業皆順遂的麥克·班寧，卻在一場襲擊後被冠上謀刺元首的罪名而遭到拘捕。這次他不僅要保護總統的安危，更要證明自己的清白。', 350, 'img\\img3.jpg', '最新推薦', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/V8-UtUTVNYE\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(4, '野蠻遊戲：全面晉級', '巨石強森,傑克·布萊克,凱文·哈特,凱倫·吉蘭,尼克·強納斯,奧卡菲娜,瑟達留士·布蘭,麥蒂森·艾斯曼,摩根·特納,亞歴克斯·沃爾夫,丹尼·德維托,丹尼·葛洛佛', '英語中字', '動作與冒險', '120', '2020', '在全新的《野蠻遊戲：全面晉級》中，前一集角色再度聚首，但遊戲的格局卻早已改變！當四位主角重返「野蠻遊戲」試圖要拯救自己的小命時，他們意外發現這個遊戲規則不僅改變，這一次，所有的玩家都必須去突破更多未探索過的全新領域；從荒蕪的沙漠，到無人之境的雪山，他們必須拼盡全力，才能逃離這個全世界最危險的遊戲，回到現實！', 350, 'img\\img4.jpg', '最新推薦', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/rGtcbciwSt0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(5, '鋼鐵勳章', '山繆·傑克森', '英語中字', '劇情', '115', '2020', '威廉·皮森伯格（傑瑞米·爾文飾演）是空軍空降救援醫療兵，在越戰中解救超過六十位弟兄，最後甚至放棄搭直升機離開戰場的機會，選擇繼續救援被留下的士兵們，最後被敵軍子彈射殺，為同袍犧牲自己寶貴的生命。他的英雄事蹟，讓他被授與士兵所能得到的最高榮譽──國會榮譽勳章，以此獎勵他所做出超越義務外的英勇作為。不過在勳章頒授之前，卻因為某些政治因素，皮森伯格應得的獎勵慘遭撤回。五角大廈調查員史考特·霍夫曼（賽巴斯汀·史坦飾演）被分配調查這件不公事件的原因，並在調查當中，發現腐敗的政治人物，以及他們拒絕授予勳章的個人及政治原因。這也讓史考特親自拜訪當年戰役中的生還者們，大家團結一心，開始為皮森伯格爭取他應得的榮譽...。', 350, 'img\\img5.jpg', '最新推薦', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/LA7lIXm8XXM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(6, '大畫特務', '權相佑, 鄭俊鎬, 黃雨瑟惠', '韓語中字', '劇情', '109', '2020', '韓國首周票房笑出3.5億台幣，榮登票房亞軍！ 韓國國家情報局秘密組織「防牌鳶」，專將孤苦無依的小孩培訓成頂尖特務，其中身手矯健的特務阿俊（權相佑飾）由魔鬼教頭千德奎（鄭俊鎬飾）親自訓練，他快狠準的身手一舉殲滅無數罪犯，成為恐怖份子都聞之喪膽的金牌特務...然阿俊心裡卻有個小小夢想－成為超人氣網路漫畫家！ 於是，他決定在一次任務中詐死脫身，隱姓埋名努力畫畫15年，但他的漫畫卻被網民嫌到爆，甚至被譏為靠太太美娜（黃雨瑟惠飾）養的中年魯蛇。 某夜鬱鬱不得志的阿俊藉酒消愁，將他的特務人生畫成漫畫上傳一夕爆紅，無心之舉掀起網民空前轟動，更引來情報局和恐怖份子的大追殺...', 350, 'img\\img6.jpg', '最新推薦', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/CKPF3Ziy5SI\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(7, '全境失控', '大澤隆夫, 岩田剛典, 賀來賢人, 廣瀨愛麗絲', '日語中字', '劇情', '131', '2020', '2030年的日本，支撐人類生活的人工智能「希望」開發者桐生浩介（大澤隆夫飾演），由於成就獲得肯定，讓他帶著女兒重返久違的日本。當桐生受到英雄般的盛大歡迎時，「希望」卻突然全面失控，甚至開始以合理與否，來決定人類的生存價值，並對無生存價值的民眾展開殺戮。警察廳天才搜查官櫻庭（岩田剛典飾演），一口咬定桐生是讓「希望」失控的恐怖份子，並透過密佈在全日本的監視網，追捕展開逃亡的桐生。 另一方面，「希望」的管理負責人西村（賀來賢人飾演），也為了澄清桐生的清白而四處奔走；轄區資深刑警合田（三浦友和飾演）與搜查一課菜鳥刑警奧瀨（廣瀨愛麗絲飾演）也積極調查。當日本陷入一片混亂之際，拼死逃亡的桐生，能否證明自己的清白？而又是什麼原因，造成「希望」的全面失控呢？', 350, 'img\\img7.jpg', '<i class=\"las la-award\"></i>本周排行第1名', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/vfQteNJBtSk\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(8, '停屍姦', '艾芭·瑞巴斯, 克里斯提安·瓦倫西亞, 艾柏特·嘉寶, 伯納特·紹梅爾', '西班牙語中字', '劇情', '74', '2017', '氣質出眾又貌美的知名影星安娜弗利茲意外身亡的消息震撼影壇。三名年輕人精蟲衝腦，闖進停屍間想目睹她赤裸的身體並與屍體發生性關係。他們萬萬沒想到，冰冷的屍體竟有了溫度！', 350, 'img\\img8.jpg', '<i class=\"las la-award\"></i>本周排行第2名', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/u5IXxDowsmc\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(9, '墊底辣妹', '有村架純, 伊藤淳史, 吉田羊, 野村周平', '日語中字', '劇情', '116', '2015', '由真人真事改編，只有小學程度不愛讀書的辣妹高中生-彩加，遇上一位補習班老師，在他的獨特教法與自己的努力下，從全校墊底一舉考上知名學府-慶應大學，也同時改變與家人的緊張關係，扭轉自己的人生！', 350, 'img\\img9.jpg', '<i class=\"las la-award\"></i>本周排行第3名', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/7yq8oGEharU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(10, '小丑', '瓦昆·菲尼克斯,勞勃·狄尼洛,薩琪·畢茲,法蘭西絲·康諾', '英語中字', '劇情', '121', '2019', '“小丑” 是一部原創的獨立故事。主人翁是亞瑟佛萊克 (華堅馮力士飾)，一個被社會遺棄的人。他的故事不只是一個寫實的角色研究，更代表一個意義深遠的警示。', 350, 'img\\img10.jpg', '<i class=\"las la-award\"></i>本周排行第4名', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Eoook2Ee6q0\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(11, '鋒迴路轉', '丹尼爾 克雷格, 克里斯 伊凡, 安娜 德 哈瑪絲, 克里斯多夫 普拉瑪', '英語中字', '喜劇', '130', '2019', '知名犯罪小說作家哈蘭（克里斯多夫普拉瑪 飾）在慶祝85歲大壽後，被發現死於自己的豪宅內，名偵探白朗（丹尼爾克雷格 飾）收到一個神祕委託，表示哈蘭的死因並非只是單純的自殺，死因並不單純，於是白朗將對家族成員一一展開調查，解決這場錯綜複雜的謀殺疑雲。', 350, 'img\\img11.jpg', '<i class=\"las la-award\"></i>本周排行第5名', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/5v4mP9ENxWo\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(12, '寄生上流', '康昊宋, 善均李, 宇植崔, 汝貞曹, 惠珍張, 素丹朴', '韓語中字', '劇情', '131', '2019', '一家四口全是無業遊民的爸爸基澤成天遊手好閒，直到積極向上的長子基宇靠著偽造的文憑來到富豪朴社長的家應徵家教，兩個天差地遠的家庭因而被捲入一連串意外事件中……', 350, 'img\\img12.png', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/PQU0_2CBM0M\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(13, '決戰中途島', '艾德·斯克林, 派翠克·威爾遜, 伍迪·哈里森, 盧克·伊萬斯', '英語中字', '劇情', '136', '2019', '打造1942年二次世界大戰期間，著名的太平洋戰爭中一場關鍵性戰役--史上聞名的「中途島戰役」，此戰被認為是大戰中扭轉局勢的轉捩點！ 本片集結好萊塢重量級卡司一同參與，導演羅蘭·艾默瑞克在磅礡戰爭中更試圖添入人性，能展現恢弘史詩，又兼顧私人情感敘事，也強調戰時日方情況，導演希望讓觀眾了解：「發動戰爭的政客從來不打仗。最終付出代價都是普通士兵。在我看來，在中途島戰役中展現出日軍和美軍一樣值得尊敬十分重要！」', 350, 'img\\img13.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/WTFJTZIqO3c\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(14, '大囍臨門', '豬哥亮, 寇世勳, 林美秀, 林心如', '繁體中文', '喜劇', '103', '2015', '32歲的淑芬（林心如 飾）終於要結婚了！不料，淑芬的魯小老爸李金爽（豬哥亮 飾）看這大陸憨女婿高飛（李東學 飾）是越看越不爽，越看越不捨將女兒外銷，開始出奇招鬥女婿。疼女兒的盧小老爸規矩訂得比101大樓還要高！聘金的數字、迎娶的人數、流水席的禮數、婚禮的禁忌，要說到完口水都沒沫。好家在大囍媒婆錢頌伊（林美秀 飾）有PRO級手腕、Everybody嚨愛；眼看新娘就要娶進門，卻突然哭跑了！安娘喂～吶ㄟ安捏？', 350, 'img\\img14.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Vk2_OuHUspc\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(15, '人面魚：紅衣小女孩外傳', '吳至璿, 張書偉, 鄭人碩, 高慧君, 龍劭華, 徐若瑄', '繁體中文', '恐怖', '114', '2018', '國片影史破億恐怖片《紅衣小女孩》系列外傳電影，原班團隊打造更駭人的魔神仔宇宙 ★2018台北金馬影展世界首映 全台票房大破七千萬 台灣真實靈異「人面魚」鄉野傳說改編 紅衣魔神仔三部曲最終章 刑事組警官阿忠（張書偉 飾）帶著一樁離奇的滅門血案找上虎爺乩身志成（鄭人碩 飾），志成決定對兇嫌執行驅魔儀式「炸魔神仔」，未料棄置的吳郭魚屍嘴中竟吐出一條遭附身的小魚，被男孩家豪（吳至璿 飾）撿回家飼養。一連串的怪事隨即開始發生，母親雅惠（徐若瑄 飾）更是舉止怪異，陷入瘋狂。為拯救母親，家豪向虎爺求救，他們原以為化解了人面魚的怨念就能阻止這一切，沒想到更難解的仇恨，正潛伏在森林裡等著他們…', 350, 'img\\img15.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/cMdnis90-gg\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(16, '角頭2：王者再起', '王識賢, 鄒兆龍, 鄭人碩, 高捷', '繁體中文', '劇情', '126', '2018', '仁哥（王識賢 飾演）面對曾經的好兄弟，如今卻「走味」變成『最換帖的敵人』的劉健（鄒兆龍 飾演）重現江湖，不斷挑起紛爭與衝突，破壞仁哥的忍耐與努力維持的和平。看著劉健的事業版圖逐漸擴大，身邊的兄弟為了情義有所犧牲，仁哥終究按耐不住釋放出心中的猛獸，與亦敵亦友的劉健正式開戰，為江湖掀起腥風血雨的廝殺。', 350, 'img\\img16.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/jvzaG5pZMxw\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(17, '復仇者聯盟: 終局之戰', '小勞勃·道尼,克里斯·伊凡,馬克·盧法洛,克里斯·漢斯沃,史嘉蕾·喬韓森,傑瑞米·雷納,唐·奇鐸,保羅·路德,布麗·拉森,凱倫·吉蘭,達娜·古瑞拉,布萊德利·庫柏\r\n,喬許·布洛林', '英語中文', '動作與冒險', '181', '2019', '復仇者聯盟系列最終章！灰飛煙毀後的世界該如何走下去，是否還有逆轉結局的機會？一切都將在此揭曉。 接續《復仇者聯盟3：無限之戰》，薩諾斯彈指間毀滅宇宙一半的生物後，僅存的復仇者們要如何重整旗鼓，背水一戰，為僅存的信念而戰。在《復仇者聯盟3：無限之戰》中未現身的鷹眼、蟻人，皆在本片登場，《驚奇隊長》也加入戰局。', 350, 'img\\img17.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/ZrB7EdfPBJU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(18, '機密行動：我們的辭典', '啟相尹, 洪發金, 海真柳', '韓語中文', '劇情', '135', '2019', '金判秀被戲院開除後，為了兒子的學費而不得去行竊別人的包包，不料卻失風被抓，包包的主人正巧是朝鮮語學會的代表柳正煥。判秀陰錯陽地獲得在朝鮮語學會工作的機會，但面對有前科又是文盲的判秀，正煥在百般不願下提出某個條件，要求判秀要去實行才能成為朝鮮語學會的一員。人生第一次閱讀文字的判秀，開始明白為何學會的人要收集朝鮮語的原因，也深刻了解到母語的重要性；而在判秀的幫助下，收集朝鮮語的過程變得更加順利，正煥也體會到「我們」這一詞的意義。在所剩不多的時間中，他們必須在日本帝國高壓的監視下完成屬於朝鮮的字典……在母語被禁止的時代，「話」與「心」匯集成我們的母語字典。', 350, 'img\\img18.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/2RWXsmlsT9U\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(19, '82年生的金智英', '裕美 鄭, 劉 孔', '韓語中文', '劇情', '118', '2019', '金智英（鄭裕美飾）是個30多歲的平凡韓國女性，她常因現實生活中全職家庭主婦和母親的身分而感到苦惱。雖然與所愛的男人結婚，辛苦養育女兒的生活迫使她放棄許多事，但智英仍相信自己很滿足於現狀。然而，智英的丈夫大賢（孔劉飾）發現她的精神狀況比想像中還要嚴重。擔心的大賢決定求助心理諮商師，他告訴醫生：「我妻子變成其他人了。」智英說話時，有時就像自己的母親、她最好的朋友，甚至還有她已離開人世的奶奶……', 350, 'img\\img19.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/x11LgXZm63c\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(20, '總舖師', '楊祐寧, 夏于喬, 林美秀', '繁體中文', '喜劇', '144', '2013', '20多年前，台灣辦桌界有三大傳奇「總舖師」，被鄉民們尊稱「人、鬼、神」三霸，稱霸北、中、南！北—「憨人師」、中—「鬼頭師」、南—「蒼蠅師」。近代辦桌文化式微，辦桌三霸匿跡。蒼蠅師想將手路菜（拿手菜）秘訣交給獨生女詹小婉，無奈小婉只想當明星，美美地出現在螢光幕前。因緣際會，小婉從台北跑路回台南後，不但認識了「料理醫生」葉如海，心地善良的小婉，還為了達成一對老夫妻的夢想，決定做出一桌「古早菜」！', 350, 'img\\img20.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Vn6HgAQXaSQ\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(21, '超級大國民 數位修復版', '林揚, 蘇明明, 陳秋燕, 邢峰, 柯一正, 丹陽, 陳國鈞, 王瑞, 鄭偉元, 蘇炳憲, 郭成弼, 葉罔市, 張淑瑜, 游定國', '繁體中文', '紀錄片', '120', '1996', '台灣新浪潮導演萬仁的超級三部曲之二 歷史的迷霧散了，景物終於能清晰看見，但是，為什麼都含著眼淚？戒嚴法「懲治判亂條例」第二條第一款唯一死刑 坐牢多年的政治犯許毅生，出獄後因內疚而閉居養老院，與當代的臺灣社會脫節約三十年。他年輕時參加讀書會被捕，不禁嚴刑拷打下而招供一位朋友，使其被判處極刑，長年深埋內心的愧疚，致使許毅生結束隱居生活，希望在臨終前，找到朋友的荒墳，向其鄭重致歉。整個尋訪的旅程中，他看見的不只是「白色恐怖」年代的昔日足跡，還有解嚴後臺灣的種種社會政治景況。濃烈悲情的《超級大國民》以迄今仍充滿誤解的「白色恐怖」主題，刻劃一位老政治犯重返現實的旅程；他在時間與空間、歷史與城市、記憶與現實之間恍惚來回。萬仁不但針對國內政治團體長久刻意忽略、迴避、曲解的「白色恐怖」，更以五○年代理想主義的政治犯，尖銳對比九○年代選舉政治裡的黑金政客，更以沉而厚的筆觸，將「內省」與「贖罪」的母題推上詩意的高峰，獲得最高的藝術成就。', 350, 'img\\img21.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/L8UxPMxCX5k\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(22, '艋舺之江湖再現', '奇鳴 楊, 曉涵 周, 鐙輝 黃', '繁體中文', '動作與冒險', '84', '2018', '一個中國東北青年來到台灣尋親，想要找到他爺爺，卻不小心捲入了幫派爭鬥；陰差陽錯之下遇見一位女孩，認定她就是他的今生真愛……', 350, 'img\\img22.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/KgReyXLH51A\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(23, '愛情算不算', '周幼婷, 張翰, 陸弈靜, 布魯斯', '繁體中文', '劇情', '97', '2015', '旅居日本八年的靜慧接受出版社的邀請，回到出生地大稻埕進行圖文集的攝影工作，沒想到人到還沒到家，就被母親抓去相親。 一直在美國工作的電腦工程師正雄，因公返台，家人積極安排他相親，希望能找個好對象結婚並且留在台灣，正雄對相親不置可否，唯一的堅持就是結婚後還是要回美國。 看似沒交集的兩人在一個相親場合認識了，更巧的是，回台不住家裡的正雄，正好住進了靜慧母親在大稻埕經營的旅館…但兩人的緣份似乎在更早之前就開始了… 恐婚的靜慧跟想婚的正雄兩人愈走愈近，但靜慧的母親反對，因為邱家祖訓不得與姓施的聯姻!!!!!!!! 另外，八年前被靜慧逃婚另娶他人的阿龍，也離婚回來了，希望能跟靜慧再續前緣… 同時，遊蕩在大橋頭的小邱總是神出鬼沒，特愛牽紅線。究竟到底誰才是靜慧的命定的戀人？月老小邱的紅線能否將兩人緊緊繫住？', 350, 'img\\img23.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/0vWzKx8NwbI\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>'),
(24, '女朋友。男朋友', '桂綸鎂, 張孝全, 鳳小岳', '繁體中文', '劇情', '105', '2012', '1980年代那個尚未開放的台灣，年少輕狂的青年張牙舞爪，恣意揮灑青春，他們衝撞體制、反抗教條、忤逆規範，他們不肯妥協。17歲的美寶（桂綸鎂飾）面對阿仁（鳳小岳飾）的熱烈追求無動於衷，因為她始終偷偷愛慕著阿良（張孝全飾）。然而，面對阿良令人難以理解的木訥，美寶漸漸接受了阿仁的追求... 畢業後，三人離開了玉蘭花園的老家，來到城市生活，紛紛踏上了學運廣場大聲疾呼，宣揚著他們的民主與自由等種種理想。於此同時，情感上的欺瞞與背叛，卻也讓三人曾經單純而美好的友情逐漸崩毀。 踏入社會、失去聯繫多年的美寶與阿良，早已被沖入時代的洪流裡，當他們再度相逢，一切已事過境遷，想起過去美好與不堪的那些，面對著年少時關於愛情的承諾，此刻兩人還有勇氣誠實面對嗎？', 350, 'img\\img24.jpg', '', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/XzwVReweL_o\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- 資料表結構 `member`
--

CREATE TABLE `member` (
  `m_id` int(255) NOT NULL,
  `m_name` varchar(20) NOT NULL,
  `m_account` varchar(255) NOT NULL,
  `m_password` varchar(255) NOT NULL,
  `m_mail` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- 傾印資料表的資料 `member`
--

INSERT INTO `member` (`m_id`, `m_name`, `m_account`, `m_password`, `m_mail`) VALUES
(1, 'asd123', 'asd123', 'asdasd', 'asd123'),
(2, '天風', 'aa324917', '24f1a0531ecebcb22a36a86f7bf121fb79b06c0a', '98ads'),
(3, 'aa123', 'aa123', 'ada14563650c1c8ea42e14b6ae9164ec93bd447c', 'aa123'),
(5, 'asdasd', 'asdasd', '85136c79cbf9fe36bb9d05d0639c70c265c18d37', 'asdasd'),
(6, 'asd1111', 'asd1111', 'fbeda12d9a412448e82f0016ce2f3772f113143b', 'asd1111'),
(7, 'asdasdasdasd', 'asdasdasd', '00ea1da4192a2030f9ae023de3b3143ed647bbab', 'asdasdasd'),
(8, 'asdasdasdasdasdasdas', 'asdasdasdasdasda', 'cb24173b9ab2f2a86c39b659ebed5ba70bb7c1b7', 'aasfasf'),
(9, 'asdasssda', 'sssa', '8f07d6c54b4f9d94b6f2cb82cc4a1f6e3f9df391', 'sss'),
(10, 'asss', 'asss', 'a6a11d21474ae8de59ab3ddbb5e8a8d0fb3b9135', 'ssss'),
(11, 'asd12', 'asd12', 'f24e84445c27fdd906c828ce26a69222329235c4', 'asdasdasdasd@gmail.com'),
(12, 'aaa', 'aaa', '7e240de74fb1ed08fa08d38063f6a6a91462a815', 'aaa@gmail.com');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`c_id`);

--
-- 資料表索引 `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`f_id`) USING BTREE;

--
-- 資料表索引 `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`m_id`) USING BTREE;

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `cart`
--
ALTER TABLE `cart`
  MODIFY `c_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `film`
--
ALTER TABLE `film`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `member`
--
ALTER TABLE `member`
  MODIFY `m_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
