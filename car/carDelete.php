<?php

session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
require_once "../index/config.php";
$tsid = $_SESSION['result']['m_name']; //session ID
$did = $_POST['did']; //接收的電影ID
$sid = $_POST['sid']; //會員 ID 

if (isset($tsid)) {
    $sql = "DELETE FROM cart WHERE f_id = ? AND m_id = ? ";
    $result = $db_link->prepare($sql);
    $result->execute(array($did, $sid));
    echo 1;
}
