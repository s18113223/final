<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
require_once "../index/config.php";
$mid = $_SESSION['result']['m_name'];
//當前登入人的ID
$sql1 = "SELECT m_id FROM member where `m_name`= ?  ";
$result1 = $db_link->prepare($sql1);
$result1->execute(array($mid));
$row1 = $result1->fetchAll(PDO::FETCH_NUM); //SQL select出來的資料
// print_r($row1);

$sql2 = "SELECT sum(c_count * 350) FROM cart WHERE m_id = ? AND c_status=1";
$result2 = $db_link->prepare($sql2);
$result2->execute(array($row1[0][0]));
$row2 = $result2->fetchAll(PDO::FETCH_NUM); //SQL select出來的資料
// echo $row2[0][0];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>購物結帳</title>
</head>
<link href="https://cdn.bootcss.com/sweetalert/1.1.3/sweetalert.min.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css" />
<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
<style>
    * {
        margin: 0px;
        padding: 0px;
        font-family: "微軟正黑體";
    }

    html::-webkit-scrollbar {
        /*隱藏滾條 */
        width: 0 !important
    }

    body {
        width: 100%;
        height: 100%;
        /* background-color: black; */


    }

    hr {
        margin: 5px;
        padding: 0px;
        width: 100%;
        height: 10px;
        background-color: red;
    }

    p {
        padding: 0px;
        margin: 0px;
    }

    #header {
        width: 100%;
        height: 700px;
        /* background-image: url(./img/img.png);
        /* background-color: rgba(255, 255, 255);  */
        /* filter: brightness(0.5); */
        border-bottom: 1px solid #e5e5e5;
    }

    #contaner {
        width: 80%;
        color: #333333;
        /* height: 500px; */
        padding-right: 15px;
        padding-left: 15px;
        margin-top: 2%;
        margin-right: 10%;
        margin-left: 10%;

        /* border-bottom: 8px solid #222; */
    }

    /* #contaner h1 {
        text-align: center;
        margin-top: 25%;
        background-color: rgba(253, 253, 253, 0.1);
        color: white;
        font-size: 45px !important;
    } */

    #btn1 {
        font-size: 20px;
        position: absolute;
        top: 10px;
        left: 87%;
    }

    #btn2 {
        font-size: 40px;
    }

    #footer {
        width: 100%;
        color: #333333;
        height: 15%;

    }

    #dirtor {
        margin-top: 10%;
        margin-left: 10%;
        color: #757575;
    }

    #title {
        width: 110%;
        margin-top: 5px 0 0 0;
        /* min-height: 60px; */
        max-height: 90px;
        color: red;
        z-index: 550;
        position: relative;
        background-color: #f3f3f3;
        border-bottom: 1px solid #e5e5e5;

    }

    #title h1 {
        margin-left: 20px;
        font-size: 50px !important;
        filter: brightness(1);
    }

    #title img {
        padding-left: 20px;
    }

    #sol {
        position: absolute;
        width: 95%;
        height: 700px;
        top: 0;
        left: 5%;
        text-align: center;
        color: white;
        padding-top: 10%;
    }

    .change {
        animation: mymove;
        background-color: rgba(0, 0, 0, 1);
    }

    .hei {
        height: 10%;
        color: #757575;
    }



    .col-12 i {
        font-size: 25px;
        display: inline-block;
    }



    #login_in,
    #inserttable {
        height: 400px;
        border-left: #222 .5px solid;
        border-right: #222 .5px solid;
        border-bottom: #222 .5px solid;
        border-radius: 10px;

    }

    ::placeholder {
        padding-left: 10px;
    }

    .total {
        width: 100%;
        height: 25%;
        position: fixed;
        top: 85%;
        right: 0px;
        bottom: 0px;
        background-color: #f3f3f3;
        padding: 7px 0 5px 0;
        text-align: center;
    }

    .table {
        text-align: center;
        
    }



    .father {
        margin-bottom: 10%;
    }

 
</style>

<body>

    <div class="row " id="title">
        
        <a href="../view/view.php"><img src="../view/img/logo.png"></a> 
        <ul class="nav m-3">
            <li class="nav-item">
                <a class="nav-link active" href="../view/view.php">首頁</a>
            </li>
        </ul>
        <button class="btn car-btn" style="position:absolute;right:17%; font-size:30px; color:black;" type="button">
            <i class="las la-shopping-cart"></i>
        </button>
        <button class="btn  dropdown-toggle " style="position:absolute;right:12%; font-size:30px; color:black; " type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="las la-user"></i>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <button class="dropdown-item" type="button" disabled>使用者: <span id="user"><?php echo $_SESSION['result']['m_name']; ?></span></button>
            <span hidden id="mid"><?php echo $row1[0][0] ?></span>
            <a href="../view/update.php" class="dropdown-item" type="button">修改資料</a>
            <a href="../view/logout.php" class="dropdown-item" type="button">登出</a>
        </div>
    </div>
    <div class="col-12 father" id="contaner">
        <h1 style="border-left: 4px solid #ff7579;">結帳區</h1>
        <div class="m-4" id="tab" class="">
            <h1>您尚未選購任何電影</h1>
        </div>

    </div>

    <div class="row total">
        <div class="col-5"></div>
        <div class="col-3"></div>
        <div class=" col-3">
        <h4 class=" select" style="line-height:1.5;">您共選購了 0 個商品</h4>
            <b><h1 id="cost">總金額 :<span id="price">0</span>元</h1></b>
        </div>
        <div class=" col-1">
            <button type="button" class="btn btn-primary getCost" style="font-size:25px;line-height:3;" onclick="show();">結帳</button>
        </div>

    </div>






    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
    <!-- 送出資料 印出購物車內容 -->
    <script>
        $(document).ready(function() {
            let id = $('#mid').text();
            console.log(id)
            $.ajax({
                type: 'POST',
                url: '../car/carSelect.php',
                dataType: 'json',
                data: {
                    id: id,
                },
                success: function(data) {
                    console.log(data);
                    console.log("success");
                    console.log(typeof(data));

                    var htm = "";
                    htm += "<table  class='table table - striped '><thead><tr><th>選擇</th><th>電影名稱</th><th>電影數量</th><th>電影售價</th><th>操作</th></tr></thead><tbody>";
                    var item = data;
                    console.log(item[0]["電影名稱"]);
                    for (var i = 0; i < item.length; i++) {
                        htm += '<tr><td><label class="check"><input type="checkbox" class="box" checked="checked" name="is_name" id=' + item[i]["電影ID"] + ' value=' + item[i]["電影數量"] + '><span class = "checkmark(this)" ></span></label> </td><td>' +
                            item[i]["電影名稱"] + '</td ><td>' +
                            item[i]["電影數量"] + '</td><td>' +
                            item[i]["電影售價"] + '</td><td>' +
                            '<button type="button" class="uptbtn btn btn-primary" id=' + "update-" + item[i]["電影ID"] + '  onclick="upt(this)" >修改數量</button>' + ' ' +
                            '<button type="button" class="delbtn btn btn-danger " id=' + "delete-" + item[i]["電影ID"] + ' onclick="del(this)">刪除整筆</button></td>';
                    }
                    htm += '</tbody></table>';
                    $("#tab").html(htm);
                    // Price(); //計算目前電影數量
                    $("#price").html(350 * Price());
                    $(".select").html("您共選購了 "+Price()+" 個商品");
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(typeof(data));
                }

            });
        });
    </script>
    <!-- 修改 購物車內容 -->
    <script>
        function upt(e) {
            let did = e.id.split("-")[1];
            let sid = $('#mid').text();
            console.log(e.id);
            console.log(did);
            $('.uptbtn').click(function() {
                swal({
                    title: "修改數量",
                    text: "請輸入數量",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: "請输入..."
                }, function(inputValue) {
                    if (inputValue === false) {
                        return false;
                    }
                    if (inputValue === "") {
                        swal.showInputError("内容不能為空！");
                        return false;
                    }
                    swal("Nice!", "你輸入的數量為：" + inputValue, "success")
                    console.log(inputValue);
                    let num = inputValue; //輸入值
                    console.log(num);
                    $.ajax({
                        type: 'POST',
                        url: 'carUpdate.php',
                        data: {
                            did: did,
                            sid: sid,
                            num: num,

                        },
                        success: function(data) {
                            console.log(1);
                            // document.write(data);
                            document.location.reload();
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            console.log(typeof(data));
                        }

                    });

                })
            });
        }
    </script>
    <!-- 刪除 購物車內容 -->
    <script>
        function del(e) {
            // console.log(e.id);
            let did = e.id.split("-")[1];
            let sid = $('#mid').text();
            // console.log(did);
            $('.delbtn').click(function() {
                swal({
                    title: "確定刪除購物車?",
                    text: "將無法恢復！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "是的，删除！",
                    cancelButtonText: "不，取消",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        swal("删除!", "您已成功删除！", "success");
                        $.ajax({
                            type: 'POST',
                            url: 'carDelete.php',
                            data: {
                                did: did,
                                sid: sid,
                            },
                            success: function(data) {
                                console.log(1);
                                // document.write(data);
                                document.location.reload();
                                $("#price").html(350 * Price());
                            },
                            error: function(XMLHttpRequest, textStatus, errorThrown) {
                                console.log(typeof(data));
                            }

                        });

                    } else {
                        swal("取消!", "購物車未刪除！", "error")
                    }
                })
            });
        }
    </script>


    <!-- 結帳功能 -->
    <script>
        $(document).change(function() { //動態計算金額
            $("#price").html(350 * Price());
            $(".select").html("您共選購了 "+Price()+" 個商品");
        })

        function Price() { //取得金額
            var c = [];
            let price = 0;
            $("input[type=checkbox]:checked").each(function() {
                c.push($(this).val());
            });
            for (i = 0; i < c.length; i++) {
                price += parseInt(c[i]);
            }
            console.log(price);
            return price;
        }


        // if ($(".box").attr('checked',true)) {
        //     obj = $("[name='is_name']");
        //    let reducer = (accumulator, currentValue) => accumulator + currentValue;
        //     console.log(1);
        //     price_value = [];
        //     let price = 0;
        //     for (i in obj) {
        //         if (obj[i].checked)

        //         price_value.push(parseInt(obj[i].value || 0));
        //     }
        //     price = price_value.reduce(reducer);
        //     // $();
        //     console.log(price);

        // } else {

        //     console.log(2);

        // }

        function show() {
            obj = $("[name='is_name']");

            check_value = [];
            // price_value = [];
            // let price = 0;
            for (i in obj) {
                if (obj[i].checked)
                    check_value.push(obj[i].id);
            }


        }
        $(".getCost").click(function show() {
            swal({
                title: "確認結帳?",
                text: "確認後訂單即成立",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "是，我要結帳！",
                cancelButtonText: "不，取消",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    swal("成功!", "您已成功結帳！", "success");
                    $.ajax({
                        type: 'POST',
                        url: 'carBuy.php',
                        data: {
                            value: check_value,
                        },
                        success: function(data) {
                            console.log(1);
                            // document.write(data);
                            document.location.reload();
                            $("#price").html(350 * Price());
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            console.log(typeof(data));
                        }

                    });

                } else {
                    swal("取消!", "結帳取消！", "error")
                }
            })
            // alert(check_value);

        });
    </script>




</body>

</html>