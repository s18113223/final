<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
	header("Location: ../index/index.php");
}
require_once "../index/config.php";
$mid = $_SESSION['result']['m_name'];
//當前登入人的ID
$sql1 = "SELECT m_id FROM member where `m_name`= ?  ";
$result1 = $db_link->prepare($sql1);
$result1->execute(array($mid));
$row1 = $result1->fetchAll(PDO::FETCH_NUM); //SQL select出來的資料
// print_r($row1);

$id = $row1[0][0]; //$id=當事人ID
// echo  $id;
// 找出當事人選甚麼電影
$sql = "SELECT DISTINCT cart.f_id,film.f_name,film.f_price,cart.m_id,cart.c_id
        FROM cart,film,member
        WHERE cart.m_id= ? AND cart.f_id=film.f_id AND cart.c_status = 1";
$result = $db_link->prepare($sql);
$result->execute(array($id));
$row = $result->fetchAll(PDO::FETCH_NUM);
// print_r($row);

// echo $row[0][0];
// 找出電影被選購幾次
$f_id = $row[0][0];
$sql3 = "SELECT f_id,c_count
	FROM cart
    WHERE m_id=? AND c_status = ?";
$result3 = $db_link->prepare($sql3);
$result3->execute(array($id, 1));
$row3 = $result3->fetchAll(PDO::FETCH_NUM);
// print_r($row3[0]);
// echo $row3[0][1];
$num = count($row3);
// echo $num;
for ($i = 0; $i < $num; $i++) {
	$array[] = array(
		"電影名稱" => $row[$i][1],
		"電影數量" => $row3[$i][1],
		"電影售價" => $row[$i][2],
		"電影ID" => $row[$i][0]
	);
}

$dataJson = json_encode($array, JSON_UNESCAPED_UNICODE);
echo $dataJson;
