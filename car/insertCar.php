<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
require_once '../index/config.php';
$id = $_POST['mid'];
$fid = $_POST['fid'];
// echo $id;
// echo $fid;
// $sql="SELECT `m_id` FROM `member` WHERE `m_name`= ? "; //找尋會員ID
// $sql1="SELECT `f_id` FROM `film` WHERE `f_id`= ? ";  //找尋電影ID
$sql2 = "INSERT INTO cart (m_id,f_id,c_count,c_status,c_date) VALUES (?,?,?,?,?)"; //新增一筆購物車資料
$sql3 = "SELECT f_id , m_id FROM cart WHERE f_id= ? AND m_id = ? "; //在購物車中找尋符合此ID和會員
$sql4 = "SELECT c_count,c_status FROM cart WHERE f_id= ? AND m_id = ? ";  //找尋這人與電影ID的數量

if (isset($_POST['mid']) && isset($_POST['fid'])) {
    // $result=$db_link->prepare($sql);
    // $result->execute(array($id));
    // $result2=$db_link->prepare($sql1);
    // $result2->execute(array($fid));

    $result4 = $db_link->prepare($sql3);
    $result4->execute(array($fid, $id));
    $row = $result4->fetchAll(PDO::FETCH_NUM);

    $result5 = $db_link->prepare($sql4);
    $result5->execute(array($fid, $id));
    $row1 = $result5->fetchAll(PDO::FETCH_NUM);


    if ($row[0] > 0) {  //如果此成員以擁有此電影
        if ($row1[0][0] >= 1) {
            if ($row1[0][1] == 2) {
                $result3 = $db_link->prepare($sql2);
                $result3->execute(array($id, $fid, 1, 1, date("Y/m/d")));
            } else {
                $sql5 = "UPDATE cart SET c_count =? WHERE f_id= ? AND m_id = ?";
                $result6 = $db_link->prepare($sql5);
                $result6->execute(array($row1[0][0] += 1, $fid, $id));
            }
        }
    } else {

        $result3 = $db_link->prepare($sql2);
        $result3->execute(array($id, $fid, 1, 1, date("Y/m/d")));
    }
}
