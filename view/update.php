<?php
session_start();

// header("Content-Type: text/html; charset=UTF-8");
require_once "../index/config.php";

$name = $_SESSION['result']['m_name'];

$sql = "SELECT * FROM member where `m_name` = ? ";
$result = $db_link->prepare($sql);
// print_r($result);
$result->execute(array($name));
$row = $result->fetch(); //SQL select出來的資料
// print_r($row);
?>

<!DOCTYPE html>
<html>

<head>
    <title>會員資料修改</title>
</head>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    * {
        margin: 0px;
        padding: 0px;
        font-family: "微軟正黑體";
    }

    html::-webkit-scrollbar {
        /*隱藏滾條 */
        width: 0 !important
    }

    body {
        width: 100%;
        height: 100%;
        /* background-color: black; */

    }

    hr {
        margin: 5px;
        padding: 0px;
        width: 100%;
        height: 10px;
        background-color: red;
    }

    p {
        padding: 0px;
        margin: 0px;
    }

    #header {
        width: 100%;
        height: 700px;
        /* background-image: url(./img/img.png);
        /* background-color: rgba(255, 255, 255);  */
        /* filter: brightness(0.5); */
        border-bottom: 1px solid #e5e5e5;
    }

    #contaner {
        width: 100%;
        color: #333333;
        height: 500px;
        border-bottom: 8px solid #222;
    }

    #contaner h1 {
        text-align: center;
        margin-top: 25%;
        background-color: rgba(253, 253, 253, 0.1);
        color: white;
        font-size: 45px !important;
    }

    #btn1 {
        font-size: 20px;
        position: absolute;
        top: 10px;
        left: 87%;
    }

    #btn2 {
        font-size: 40px;
    }

    #footer {
        width: 100%;
        color: #333333;
        height: 15%;

    }

    #dirtor {
        margin-top: 10%;
        margin-left: 10%;
        color: #757575;
    }

    #title {
        width: 110%;
        margin-top: 5px 0 0 0;
        min-height: 60px;
        max-height: 90px;
        color: red;
        z-index: 550;
        /* position: fixed; */
        background-color: #f3f3f3;
        border-bottom: 1px solid #e5e5e5;

    }

    #title h1 {
        margin-left: 20px;
        font-size: 50px !important;
        filter: brightness(1);
    }

    #title img {
        padding-left: 20px;
    }

    #sol {
        position: absolute;
        width: 95%;
        height: 700px;
        top: 0;
        left: 5%;
        text-align: center;
        color: white;
        padding-top: 10%;
    }

    .change {
        animation: mymove;
        background-color: rgba(0, 0, 0, 1);
    }

    .hei {
        height: 10%;
        color: #757575;
    }

    input {
        height: 40px;
    }


    .col-12 i {
        font-size: 25px;
        display: inline-block;
    }

    input {
        width: 70%;
        border-width: 0;
    }

    input:hover {

        border-bottom: orange 2px solid;
        transition: border-bottom .3s ease-in;
    }

    input:focus {
        outline: none;
    }


    .up {
        height: 400px;
        border-left: #222 .5px solid;
        border-right: #222 .5px solid;
        border-bottom: #222 .5px solid;
        border-radius: 10px;

    }

    ::placeholder {
        padding-left: 10px;
    }

    .btn {
        background-color: red;
        border: red;
    }

    .btn:hover {
        background-color: red;
    }
</style>

<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <div class="col-12" id="header">
        <div class="row" id="title">
            <!-- <div class="col-2" style="background-image: url(img/logo.png);background-repeat: no-repeat;" > </div> -->
            <a href="../view/view.php" ><img src="img/logo.png"></a> 
            <!-- <p style="font-size: 47px; padding-left:20px;"><b>電影網</b></p> -->
        </div>
        <div class="row " style="padding-top: 8%; ">
            <div class="col-3"></div>
            <div class="col-6 up" id="updatetable" style="padding: 2% 0 5% 10%;background-color: #f3f3f3; ">
                <h1><b>修改個人資料</b></h1>
                <form id="update" name="update" method="POST" action="updateSql.php">
                    <div class="col-12" style="padding: 20px 0 10px 0;">
                        <span><i class="las la-signature"></i></span>
                        <input id="name" name="name" style="padding-left: 10px;" value='<?php echo $row['m_name'] ?>' disabled />
                        <input id="hidden" name="hidden" value='<?php echo $_SESSION['result']['m_name']; ?>' type="hidden">
                    </div>
                    <div class="col-12" style="padding: 0px 0 10px 0;">
                        <span><i class="las la-user"></i></span>
                        <input type="text" id="acc" name="acc" placeholder='<?php echo $row['m_account'] ?>' disabled />
                    </div>
                    <div class="col-12" style="padding: 0px 0 10px 0;">
                        <span><i class="las la-lock"></i></span>
                        <input type="password" id="passwd" name="passwd" placeholder="建立密碼" required />
                    </div>
                    <div class="col-12" style="padding: 0px 0 10px 0;">
                        <span><i class="las la-envelope"></i></span>
                        <input type="email" id="mail" name="mail" placeholder='<?php echo $row['m_mail'] ?>' disabled />
                    </div>
                    <div class="row" style="float: right; padding-right:150px;">
                        <div class="col-12">
                            <button id="updateSubmit" type="submit" class="btn btn-primary">送出修改</button>
                            <a href="./view.php" class="btn btn-primary">返回</a>
                        </div>

                    </div>
                </form>

            </div>

        </div>
    </div>



    <div class="row" style="background-color: #f3f3f3;">
        <div class="col-6 hei" style="border-right: 2px  #222; ">
            <div id="dirtor">
                <h4>108年第二學年期末作業</h4>
                <h5>網頁參考:<a href="https://www.netflix.com/">Netflix</a></h5>
                <h5>&emsp;&emsp;&emsp;&emsp;&nbsp;<a href="https://play.google.com/store/movies">Google Movie</a></h5>
            </div>
        </div>
        <div class="col-6 hei" style="border-left: 2px  #222; ">
            <div id="dirtor">
                <h3>製作人:</h3>
                <h5>樹德科大資工二乙蔡銘凱</h5>
            </div>
        </div>
        <div class="col-12 hei" style=" text-align: center; padding-top:60px; ">
            <p>&copy;s18113223 蔡銘凱</p>
        </div>
    </div>
    <!-- contaner end-->


    <!-- <script>
        $("#updateSubmit").click(function() {
            var passwd = $('#passwd').val();
            var name = $('#hidden').val();
            console.log(passwd);
            console.log(name);
            if (passwd != "") {
                $.ajax({
                    type: 'POST',
                    url: 'updateSql.php',
                    data: {
                        name: name,
                        passwd: password
                    },
                    success: function(data) {
                        console.log("1");
                        document.write(data);
                        window.location.reload();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.log(typeof(data));
                    }

                });
            } else {
                console.log("3");
            }
        })
    </script> -->
 









    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>