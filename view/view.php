<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
	header("Location: ../index/index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>首頁</title>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
	<link rel="stylesheet" href="css/view.css">
	<style>




	</style>
</head>

<body>
	<?php
	// header("Content-Type: text/html; charset=UTF-8");
	require_once "../index/config.php";
	$sql = "SELECT * FROM film";
	$result = $db_link->query($sql);
	$row = $result->fetchAll(); //SQL select出來的資料
	?>
	<div class="row " id="title" style="top: 0px;">
		<img src="img/logo.png">
		<ul class="nav m-3">
			<li class="nav-item">
				<a class="nav-link active" href="#">首頁</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="allfilm.php">全部電影</a>
			</li>

		</ul>
		<a href="../car/carView.php" class="btn car-btn" style="position:absolute;right:15%; font-size:30px; color:white;" type="button">
			<i class="las la-shopping-cart"></i>
		</a>
		<button class="btn  dropdown-toggle " style="position:absolute;right:10%; font-size:30px; color:white; background-color: rgba(0,0,0,0.6);" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="las la-user"></i>
		</button>
		<div class="dropdown-menu" aria-labelledby="dropdownMenu2">
			<button class="dropdown-item" type="button" disabled>使用者: <?php echo $_SESSION['result']['m_name']; ?></button>
			<!-- model in line68 or #sol -->
			<a href="update.php" class="dropdown-item" type="button" target="_blank">修改資料</a>
			<a href="logout.php" class="dropdown-item" type="button">登出</a>
		</div>

	</div>
	<div class="col-12 p-0" id="header">
		<video id="media" loop="true" autoplay="autoplay" muted="true">
			<source type="video/mp4" src="media/theend.mp4">
			</source>
		</video>
	</div>
	<div class="row" id="sol">
		<div class="col-4 ml-1 m-0">
			<p style="font-size:3vw;"><b>強檔新片，火熱上線。</b></p><br>
			<p style="font-size:2vw;"><b>點擊詳情，搶先看</b></p><br>
			<a href="film.php?<?php echo $row[16][0] ?>" type="button" class="btn btn-secondary" id="btn2">詳細資訊</a>
		</div>

	</div>
	<!-- 最新 -->
	<div id="contaner" class="row">
		<div class="col-12">
			<div class="row">
				<div class="col-12 ">
					<h1 class="mt-1" style="background-color:rgba(0,0,0,0); float:left;"><b>最新推薦</b></h1>
				</div>
			</div>
			<div class="row" style="height: 84%; padding: 0 5px 0 5px;">
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[0][0] ?>"><img src="<?php echo $row[0][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1 " style="height: 95%;">
					<i id="plus" type="button" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[1][0] ?>"><img src="<?php echo $row[1][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[2][0] ?>"><img src="<?php echo $row[2][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[3][0] ?>"><img src="<?php echo $row[3][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[4][0] ?>"><img src="<?php echo $row[4][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[5][0] ?>"><img src="<?php echo $row[5][9] ?>" width="100%" ; height="100%;" /></a>
				</div>

			</div>


		</div>
	</div>
	<!-- contaner end-->
	<!-- 排行 -->
	<div id="contaner" class="row">
		<div class="col-12">
			<div class="row">
				<div class="col-12 ">
					<h1 class="mt-1" style="background-color:rgba(0,0,0,0); float:left;"><b>本周排行</b></h1>
				</div>
			</div>
			<div class="row" style="height: 84%; padding: 0 5px 0 5px;">
				<div class="col-12">
					<div class="row">
						<div style="width:16.7%;">
							<div class="row rank">
								<div class="col-4">
									<p style="font-size: 14.5vw;">1</p>
								</div>
								<div class="col-8"><a href="film.php?<?php echo $row[6][0] ?>"><img src="<?php echo $row[6][9] ?>" width="100%" ; height="100%;" /></a></div>
							</div>
						</div>
						<div style="width:16.7%;">
							<div class="row rank">
								<div class="col-4">
									<p style="font-size: 14.5vw;">2</p>
								</div>
								<div class="col-8"><a href="film.php?<?php echo $row[7][0] ?>"><img src="<?php echo $row[7][9] ?>" width="100%" ; height="100%;" /></a></div>
							</div>
						</div>
						<div style="width:16.7%;">
							<div class="row rank">
								<div class="col-4">
									<p style="font-size: 14.5vw;">3</p>
								</div>
								<div class="col-8"><a href="film.php?<?php echo $row[8][0] ?>"><img src="<?php echo $row[8][9] ?>" width="100%" ; height="100%;" /></a></div>
							</div>
						</div>
						<div style="width:16.7%;">
							<div class="row rank">
								<div class="col-4">
									<p style="font-size: 14.5vw;">4</p>
								</div>
								<div class="col-8"><a href="film.php?<?php echo $row[9][0] ?>"><img src="<?php echo $row[9][9] ?>" width="100%" ; height="100%;" /></a></div>
							</div>
						</div>
						<div style="width:16.6%;">
							<div class="row rank">
								<div class="col-4">
									<p style="font-size: 14.5vw;">5</p>
								</div>
								<div class="col-8"><a href="film.php?<?php echo $row[10][0] ?>"><img src="<?php echo $row[10][9] ?>" width="100%" ; height="100%;" /></a></div>
							</div>
						</div>
						<div style="width:16.6%;">
							<div class="row rank">
								<div class="col-4">
									<p style="font-size: 14.5vw;">6</p>
								</div>
								<div class="col-8"><a href="film.php?<?php echo $row[0][0] ?>"><img src="<?php echo $row[0][9] ?>" width="100%" ; height="100%;" /></a></div>
							</div>
						</div>
					</div>


				</div>

			</div>


		</div>
	</div>
	<!-- contaner end-->
	<!-- 預告推薦 -->
	<div id="contanerVideo" class="row">
		<div class="col-12" id="back" style="background-image: url(img/jk1.jpg);  background-size:auto 700px; ;background-repeat:no-repeat;background-position:150%;">
			<div class="row">
				<div class="col-7 mt-4">
					<video id="media1" loop="true" autoplay="autoplay" muted="true">
						<source type="video/mp4" src="media/joker.mp4">
						</source>
					</video>
				</div>
				<div class="col-4 mt-4 mb-4">
					<div class="mt-5">
						<p style="font-size:1vw; color:white"><b><?php echo $row[9][4] ?></b></p>
						<img src="img/jkbg1.png" width="80%" height="60%;" /><br>

						<!-- <p style="font-size:3.2vw;"><b>JOKER</b></p>-->
						<a type="button" class="btn btn-secondary" id="btn3" href="film.php?<?php echo $row[9][0] ?>">詳細資訊</a>
						<p class="mt-4" style="font-size:2vw; color:white"><b><i class="las la-award"></i>本周排行第4名</b></p><br>
						<div class="word m-0" style="font-size:1vw; color:white"><?php echo $row[9][7] ?></div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- contaner end-->
	<!-- 高點閱 -->
	<div id="contaner" class="row">
		<div class="col-12">
			<div class="row">
				<div class="col-12 ">
					<h1 class="mt-1" style="background-color:rgba(0,0,0,0); float:left;"><b>經典台灣電影</b></h1>
				</div>
			</div>
			<div class="row" style="height: 84%; padding: 0 5px 0 5px;">
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[14][0] ?>"><img src="<?php echo $row[14][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1 " style="height: 95%;">
					<i id="plus" type="button" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[15][0] ?>"><img src="<?php echo $row[15][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[21][0] ?>"><img src="<?php echo $row[21][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[20][0] ?>"><img src="<?php echo $row[20][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[22][0] ?>"><img src="<?php echo $row[22][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
				<div class="col-2 new p-0 pr-1" style="height: 95%;">
					<i id="plus" class="las la-plus-circle"></i>
					<a href="film.php?<?php echo $row[23][0] ?>"><img src="<?php echo $row[23][9] ?>" width="100%" ; height="100%;" /></a>
				</div>
			</div>
		</div>
	</div>
	<!-- contaner end-->
	<div id="footer" class="row">
		<div class="col-6" style="border-right: 2px  #222; ">
			<div id="dirtor">
				<h4>108年第二學年期末作業</h4>
				<h5>網頁參考:<a href="https://www.netflix.com/">Netflix</a></h5>
				<h5>&emsp;&emsp;&emsp;&emsp;&nbsp;<a href="https://play.google.com/store/movies">Google Movie</a></h5>
			</div>
		</div>
		<div class="col-6" style="border-left: 2px  #222; ">
			<div id="dirtor">
				<h3>製作人:</h3>
				<h5>樹德科大資工二乙蔡銘凱</h5>
			</div>
		</div>
		<div class="col-12" style=" text-align: center;">
			<p>&copy;s18113223 蔡銘凱</p>
		</div>
	</div>
	<!-- contaner end-->

	<script>
		$(document).on("scroll", function() { //滾動時NAV背景改變
			if ($(window).scrollTop() > 50) {
				$("#title").addClass("change");
			} else {
				//remove the background property so it comes transparent again (defined in your css)
				$("#title").removeClass("change");
			}
		});
	</script>
	<script>
		$(document).ready(function() {
			$(".new").hover(function() {
				$(this).addClass("animation", "icon 3s");
			});
		});
		$(document).ready(function() {
			$(".rank").hover(function() {
				$(this).addClass("blockhover");
			}, function() {
				$(this).removeClass("blockhover");

			});
		});
		$(document).ready(function() {
			$(".rank").hover(function() {
				$(this).parent('div').addClass("zindex");
			}, function() {
				$(this).parent('div').removeClass("zindex");

			});
		});
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>