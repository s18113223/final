<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>首頁</title>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="css/all.css">
    <style>




    </style>
</head>

<body>

    <div class="row " id="title" style="top: 0px;">
        <img src="img/logo.png">
        <ul class="nav m-3">
            <li class="nav-item">
                <a class="nav-link active" href="view.php">首頁</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">全部電影</a>
            </li>

        </ul>
        <a href="../car/carView.php" class="btn car-btn" style="position:absolute;right:15%; font-size:30px; color:white;" type="button">
            <i class="las la-shopping-cart"></i>
        </a>
        <button class="btn  dropdown-toggle " style="position:absolute;right:10%; font-size:30px; color:white; background-color: rgba(0,0,0,0.6);" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="las la-user"></i>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <button class="dropdown-item" type="button" disabled>使用者: <?php echo $_SESSION['result']['m_name']; ?></button>
            <!-- model in line68 or #sol -->
            <a href="update.php" class="dropdown-item" type="button" target="_blank">修改資料</a>
            <a href="logout.php" class="dropdown-item" type="button">登出</a>
        </div>

    </div>
    <div class="row" style="height: 60px;">
        <div class="col-12"></div>
    </div>
    <!-- 最新 -->
    <div id="inner">

    </div>


    <div id="" class="row">
        <div class="col-6" style="border-right: 2px  #222; ">
            <div id="dirtor">
                <h4>108年第二學年期末作業</h4>
                <h5>網頁參考:<a href="https://www.netflix.com/">Netflix</a></h5>
                <h5>&emsp;&emsp;&emsp;&emsp;&nbsp;<a href="https://play.google.com/store/movies">Google Movie</a></h5>
            </div>
        </div>
        <div class="col-6" style="border-left: 2px  #222; ">
            <div id="dirtor">
                <h3>製作人:</h3>
                <h5>樹德科大資工二乙蔡銘凱</h5>
            </div>
        </div>
        <div class="col-12" style=" text-align: center;">
            <p>&copy;s18113223 蔡銘凱</p>
        </div>
    </div>
    <!-- contaner end-->

    <script>
        $(document).on("scroll", function() { //滾動時NAV背景改變
            if ($(window).scrollTop() > 50) {
                $("#title").addClass("change");
            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $("#title").removeClass("change");
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".new").hover(function() {
                $(this).addClass("animation", "icon 3s");
            });
        });
        $(document).ready(function() {
            $(".rank").hover(function() {
                $(this).addClass("blockhover");
            }, function() {
                $(this).removeClass("blockhover");

            });
        });
        $(document).ready(function() {
            $(".rank").hover(function() {
                $(this).parent('div').addClass("zindex");
            }, function() {
                $(this).parent('div').removeClass("zindex");

            });
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $.ajax({
                type: 'POST',
                url: 'allSql.php',
                dataType: 'json',
                success: function(data) {

                    console.log("success");
                    // document.write(data);     
                    var item = data;
                    // console.log(typeof(data));
                    //  console.log(data);
                    // console.log(data[0][1]);
                    htm = '<div id="contaner"><div  class="row" style="height: 84%; padding: 0 5px 0 5px;"><div class="col-12"><h1 class="m-0 mb-3">全部電影</h1></div>';
                    for (let i = 0; i < item.length; i++) {
                        htm += '<div class="col-2 new p-0 pr-1" style="height: 50%;">' +
                            '<i id="plus" class="las la-plus-circle"></i>' +
                            '<a href="film.php?' + item[i][0] + '" ><img src="' + item[i][9] + '" width="100%"; height="100%;"/></a></div>';
                    }
                    htm += '</div></div>';

                    $('#inner').html(htm);

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(typeof(data));
                }

            });
        })
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>