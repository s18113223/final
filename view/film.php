<?php
session_start();
if (!isset($_SESSION['result']['m_name'])) {
    header("Location: ../index/index.php");
}
?>
<?php
// header("Content-Type: text/html; charset=UTF-8");
require_once "../index/config.php";
$mid = $_SESSION['result']['m_name'];
// echo $mid;
$str = $_SERVER['REQUEST_URI'];;
$sec = explode("film.php?", $str);
$id = $sec[1];
$sql = "SELECT * FROM film where `f_id`= $id ";
$result = $db_link->query($sql);
$row = $result->fetchAll(); //SQL select出來的資料
// print_r($row);
$sql1 = "SELECT m_id FROM member where `m_name`= ?  ";
$result1 = $db_link->prepare($sql1);
$result1->execute(array($mid));
$row1 = $result1->fetchAll(); //SQL select出來的資料
// print_r($row1);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo "<title>" . $row[0][1] . "</title>" ?>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <link rel="stylesheet" href="css/film.css">
    <style>

    </style>
</head>

<body>
    <div class="row " id="title">
        <img src="img/logo.png">
        <ul class="nav m-3">
            <li class="nav-item">
                <a class="nav-link active" href="view.php">首頁</a>
            </li>
        </ul>
        <a href="../car/carView.php" class="btn car-btn" style="position:absolute;right:15%; font-size:30px; color:white;" type="button">
            <i class="las la-shopping-cart"></i>
        </a>
        <button class="btn  dropdown-toggle " style="position:absolute;right:10%; font-size:30px; color:white; background-color: rgba(0,0,0,0.6);" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="las la-user"></i>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <button class="dropdown-item" type="button" disabled>使用者: <span id="user"><?php echo $_SESSION['result']['m_name']; ?></span><span hidden id="fid"><?php echo $row[0][0] ?></span></button>
            <span hidden id="mid"><?php echo $row1[0][0] ?></span>
            <a href="update.php" class="dropdown-item" type="button" target="_blank">修改資料</a>
            <a href="logout.php" class="dropdown-item" type="button">登出</a>
        </div>
    </div>
    <div id="title2"></div>
    <div id="contaner">
        <div class="mt-3 ml-3 mr-3">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">電影詳情</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="row" style="padding:2% 0 2% 2%; border-bottom: 8px solid #222;">
                        <div class="col-1"></div>
                        <div class="col-3">
                            <img src="<?php echo $row[0][9] ?>" width="100%" height="700px;" />
                        </div>
                        <div class="col-1"></div>
                        <div class="col-6" style="color: white;">
                            <div class="row " style="margin-top: 8%;">
                                <div class="col-12">
                                    <p style="font-size:2vw;"><b><?php echo $row[0][10] ?></b></p>

                                </div>
                            </div>
                            <div class="row mt-4 ">
                                <div class="col-12"><b class="" style="font-size:4vw;"><?php echo $row[0][1] ?></b></div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-3"><b class="" style="font-size:1vw;">上映年份:<?php echo $row[0][6] ?></b></div>
                                <div class="col-3"><b class="" style="font-size:1vw;">語言:<?php echo $row[0][3] ?></b></div>
                            </div>
                            <div class="row mt-1">
                                <div class="col-3"><b class="" style="font-size:1vw;">類型:<?php echo $row[0][4] ?></b></div>
                                <div class="col-3"><b class="" style="font-size:1vw;">片長:<?php echo $row[0][5] ?>分鐘</b></div>
                            </div>

                            <div class="row mt-5">
                                <div class="col-2"><b style="font-size:1.5vw;">主演 </b></div>
                                <div class="col-12"><b style="font-size:1.5vw;"><?php echo $row[0][2] ?></b></div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-6"><b style="font-size:3vw;color:grey">售價：<span id="price"><?php echo $row[0][8] ?></span>NTD</b></div>
                                <div class="col-10">
                                    <button id="buycar" class="btn btn-danger" style="font-size:2.5vw; float:right;">加入購物車</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- 基本資料end -->
                    <!-- 預告 -->
                    <div class="row">
                        <div class="col-12">
                            <h2 style="margin:5% 0 2% 0;padding-left: 5%;width:100%;">劇情預告<span>/About The Plot</span></h2>
                            <div class="video-container">
                                <?php echo $row[0][11] ?>
                            </div>
                            <!-- <video id="media1" loop="true" autoplay="autoplay"  muted="true">
                                <source type="video/mp4" src="<?php echo $row[0][11] ?>"></source>
                            </video> -->
                        </div>
                        <div class="col-12" style="padding-left: 5%; margin-bottom:5%;">
                            <h2 style="margin-top: 5%;width:100%;">劇情簡介<span>/ABOUT THE STORY</span></h2>
                            <div class="word"><?php echo $row[0][7] ?></div>
                        </div>
                    </div>
                    <!-- 預告end -->
                </div>
            </div>
        </div>


    </div>






    <div id="footer" class="row">
        <div class="col-6" style="border-right: 2px  #222; ">
            <div id="dirtor">
                <h4>108年第二學年期末作業</h4>
                <h5>網頁參考:<a href="https://www.netflix.com/">Netflix</a></h5>
                <h5>&emsp;&emsp;&emsp;&emsp;&nbsp;<a href="https://play.google.com/store/movies">Google Movie</a></h5>
            </div>
        </div>
        <div class="col-6" style="border-left: 2px  #222; ">
            <div id="dirtor">
                <h3>製作人:</h3>
                <h5>樹德科大資工二乙蔡銘凱</h5>
            </div>
        </div>
        <div class="col-12" style=" text-align: center;">
            <p>&copy;s18113223 蔡銘凱</p>
        </div>
    </div>
    <!-- contaner end-->

    <script>
        $(document).on("scroll", function() { //滾動時NAV背景改變
            if ($(window).scrollTop() > 50) {
                $("#title").addClass("change");
            } else {
                //remove the background property so it comes transparent again (defined in your css)
                $("#title").removeClass("change");
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".new").hover(function() {
                $(this).addClass("animation", "icon 3s");
            });
        });
        $(document).ready(function() {
            $(".rank").hover(function() {
                $(this).addClass("blockhover");
            }, function() {
                $(this).removeClass("blockhover");

            });
        });
        $(document).ready(function() {
            $(".rank").hover(function() {
                $(this).parent('div').addClass("zindex");
            }, function() {
                $(this).parent('div').removeClass("zindex");

            });
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        let mid = $('#mid').text();
        let fid = $('#fid').text();
        let fprice = $('#price').text();
        console.log(mid);
        console.log(fid);
        console.log(fprice);
        $('#buycar').click(function() {
            $.ajax({
                type: 'POST',
                url: '../car/insertCar.php',
                data: {
                    mid: mid,
                    fid: fid,
                },
                success: function(data) {
                    swal("成功!", "已加入購物車", "success");
                    console.log("success");
                    // document.write(data);     

                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log(typeof(data));
                }

            });

        })
    </script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>