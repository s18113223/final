<!DOCTYPE html>
<html>

<head>
	<title>首頁</title>
</head>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="css/index.css">

<body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<div class="row" id="title">
		<img src="img/logo.png">
		<a href="login.php" type="button" class="btn btn-danger" id="btn1"  ><b>登入</b></a>

	</div>
	<div class="col-12" id="header">


	</div>
	<div class="row" id="sol">
		<div class="col-8">
			<p style="font-size: 70px;"><b>強檔新片，火熱上線。</b></p><br>
			<p style="font-size: 60px;"><b>立即註冊，搶先觀看</b></p><br>
			<a href="login.php" type="button" class="btn btn-danger" id="btn2" >立即註冊</a>
		</div>
	</div>

	<!-- contaner -->
	<div id="contaner" class="row">
		<div class="col-6">
			<h1 style="font-size: 40px;"><b>強檔新片出租</b></h1>
		</div>
		<div class="col-6">
			<img src="img/index1.png" />
		</div>
	</div>
	<!-- contaner end-->
	<!-- contaner -->
	<div id="contaner" class="row">
		<div class="col-6">
			<img src="img/index2.png" />
		</div>
		<div class="col-6">
			<h1 style="font-size: 40px;"><b>完善租、購片流程</b></h1>
		</div>
	</div>
	<!-- contaner end-->
	<!-- contaner -->
	<div id="contaner" class="row">
		<div class="col-6">
			<h1 style="font-size: 40px;"><b>價格親民</b></h1>
		</div>
		<div class="col-6">
			<img src="img/index3.jpg" />
		</div>
	</div>
	<!-- contaner end-->
	<div id="footer" class="row">
		<div class="col-6" style="border-right: 2px  #222; ">
			<div id="dirtor">
				<h4>108年第二學年期末作業</h4>
				<h5>網頁參考:<a href="https://www.netflix.com/">Netflix</a></h5>
				<h5>&emsp;&emsp;&emsp;&emsp;&nbsp;<a href="https://play.google.com/store/movies">Google Movie</a></h5>
			</div>
		</div>
		<div class="col-6" style="border-left: 2px  #222; ">
			<div id="dirtor">
				<h3>製作人:</h3>
				<h5>樹德科大資工二乙蔡銘凱</h5>
			</div>
		</div>
		<div class="col-12" style=" text-align: center; padding-top:60px; ">
			<p>&copy;s18113223 蔡銘凱</p>
		</div>
	</div>
	<!-- contaner end-->







	<script>
		$(document).on("scroll", function() {
			if ($(window).scrollTop() > 50) {
				$("#title").addClass("change");
			} else {
				//remove the background property so it comes transparent again (defined in your css)
				$("#title").removeClass("change");
			}
		});
	</script>

	<!-- <script>
 $(document).ready(function(){
    $("#loginSubmit").click(function(){
        if($("#account").val()=="" || $("#password").val()==""){
            alert("你尚未填寫帳號或密碼");
            eval("document.login['account'].focus()");       
        }else{
            document.login.submit();
        }
    })
 })

		

</script> -->


	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>